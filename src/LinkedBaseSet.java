import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LinkedBaseSet implements IntSet {

    private Node head;

    LinkedBaseSet() {
        this.head = null;
    }

    @Override
    public int size() {
        int size = 0;
        Node current = head;
        while (current != null) {
            size++;
            current = current.getNext();
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

    @Override
    public boolean add(int newEntry) {
        if (this.contains(newEntry)) {
            System.out.printf("WARN: List already contains \"%d\", cannot add.\n", newEntry);
            return false;
        }
        Node node = new Node(newEntry);
        if (head == null) {
            head = node;
        } else {
            Node last = head;
            while (last.getNext() != null) {
                last = last.getNext();
            }
            last.setNext(node);
        }
        return true;
    }

    @Override
    public boolean remove(int anEntry) {
        Node current = head;
        Node previous = current;
        while (current != null) {
            if (anEntry == current.getData()) {
                if (current == head) {
                    head = head.getNext();
                } else {
                    previous.setNext(current.getNext());
                }
                return true;
            }
            previous = current;
            current = current.getNext();
        }
        return false;
    }

    @Override
    public void clear() {
        this.head = null;
    }

    @Override
    public boolean contains(int anEntry) {
        Node current = head;
        while (current != null) {
            if (anEntry == current.getData()) {
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    @Override
    public int[] toArray() {
        List<Node> list = new ArrayList<>();
        Node current = head;
        while (current != null) {
            list.add(current);
            current = current.getNext();
        }
        return list.stream().mapToInt(Node::getData).toArray();
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        Node current = head;
        while (current != null) {
            builder.append(String.format(Locale.US, "%d,", current.getData()));
            current = current.getNext();
        }
        return builder.toString();
    }
}
