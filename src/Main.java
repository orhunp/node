public class Main {
    public static void main(String[] args) {
        IntSet set1 = new LinkedBaseSet();
        set1.add(33);
        set1.add(23);
        set1.add(14);
        set1.add(1);
        set1.add(2);
        set1.add(23);
        set1.add(18);
        set1.add(33);
        set1.add(33);
        set1.add(33);
        System.out.println(set1);
        if (set1.contains(44))
            System.out.println("INFO: Set1 contains 44");
        else
            System.out.println("INFO: Set1 doesn't contain 44");
        set1.remove(23);
        System.out.println(set1);
        int[] items = set1.toArray();
        for (int a : items)
            System.out.println(a);
    }
}
