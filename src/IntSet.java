public interface IntSet {
    int size();
    boolean isEmpty();
    boolean add(int newEntry);
    boolean remove(int anEntry);
    void clear();
    boolean contains(int anEntry);
    int[] toArray();
    String toString();
}
